'use strict';

const _ = require('lodash');

const validUrl = require('valid-url');
const URL = require('url-parse');
const UrlPattern = require('url-pattern');

const request = require('superagent');
const prefix = require('superagent-prefix');
const suffix = require('superagent-suffix');
const agent = require('superagent-use')(request);
const serializer = require('superagent-serializer');
const superagentJsonapify = require('superagent-jsonapify');

const SERIALIZER_CASES = ['upper', 'lower', 'snake', 'pascal', 'camel', 'kebab', 'constant', 'title', 'capital'];

// class CustomError extends Error {
//
// }

// const DEFAULT_METHODS = {
//   get: {
//
//   },
//   post: {
//
//   },
//   put: {
//
//   },
//   delete: {
//
//   },
//   option: {
//
//   },
//   head: {
//
//   },
//   patch: {
//
//   }
// };

class ApiRequest {
  // /**
  //  *
  //  * @param opts {Object}
  //  * @param opts.url {string}
  //  * @param opts.serialize {string}
  //  * @param opts.jsonapify {boolean}
  //  * @param opts.apiPrefix {string}
  //  * @param opts.suffix {string}
  //  * @param opts.retries {integer}
  //  * @param opts.timeout {object}
  //  * @param opts.timeout.response {integer}
  //  * @param opts.timeout.deadline {integer}
  //  */
  constructor({
    url,
    uri,
    domain,
    namespacePrefix,
    apiPrefix,
    suffix,
    pattern: {
      domainPattern,
      namespacePattern,
      apiPattern,
      resourcePattern
    },
    retries,
    timeout: {
      response,
      deadline
    },
    serialize,
    jsonapify
  }) {

    if (!arguments.length) {

    }

    if (!url || !validUrl.isUri(url)) {
      throw new Error('');
    }

    this.url = this.uri = url;
    this.parsedUrl = new URL(this.url);
    this.apiPrefix = apiPrefix;
    this.origin = this.parsedUrl.origin;
    this.prefix = this.origin + this.apiPrefix;
    agent.use(prefix(this.prefix));

    // universal
    // '(http(s)\\://)(:subdomain.):domain.:tld(\\::port)
    // (/:namespace)
    // /api(/v:major(.:minor)
    // /:resource(/:resourceId(/:subResource(/:subResourceId(/:nestedResource(/:nestedResourceId(/:rest(/*)))))))'
    const domainPatternStr = '(http(s)\\://)(:subdomain.):domain.:tld(\\::port)';
    const namespacePatternStr = '(/:namespace)';
    const apiPatternStr = '(/api(/v:major(.:minor))';
    const resourcePatternStr = '/:resource(/:resourceId(/:subResource(/:subResourceId(/:nestedResource(/:nestedResourceId(/:rest(/*)))))))';
    new UrlPattern();


    // reconnects attempts count
    this.retries = retries;
    agent.retry(this.retries);

    // reconnection timeouts
    this.responeTimeout = response; // timeout to firs response data
    this.responeDeadline = deadline; // max time for getting response data
    agent.timeout({
      response: this.responeTimeout,
      deadline: this.responeDeadline
    });

    if (suffix && _.isString(suffix)) {
      this.suffix = suffix;
      agent.use(suffix(this.suffix));
    }

    if (serialize && _.isString(serialize) && _.includes(SERIALIZER_CASES, serialize)) {
      serializer(agent, serialize);
    }

    if (jsonapify && _.isBoolean(jsonapify)) {
      superagentJsonapify(agent);
    }

    // var pattern = new UrlPattern(
    //   /^\/(api(\/v\d+)?\/)?([^\/]+)(?:\/(\d+|\w+))?(?:\/([^\/]+)(?:\/(\d+|\w+))?)?(?:\/([^\/]+)(?:\/(\d+|\w+))?)?(?:\/([^\/]+)?)?$/,
    //   ['api', 'version', 'resource', 'resourceId', 'subResource', 'subResourceId', 'other', 'otherId', 'rest']
    // );
    // '(/api)/posts(/:id(/:sub(/:rest(/*))))'

    // /^\/api\/([^\/]+)(?:\/(\d+))?$/,
    // /(^\/?\/)([^\/]+)(?:\/(\w+|\d+))?$/,
    // [api, 'resource', 'id']
    // ?  api ? resourse ? id ? subresource ? subId ? other
    // 'http://jsonplaceholder.typicode.com/api/posts/:id'
    // 'http://jsonplaceholder.typicode.com/api/posts(/:id)'
    // possible -> // 'http://jsonplaceholder.typicode.com/api/posts/:id/resource/:resourceId/nested'

    // this.url = opts.url && _.isString(opts.url) && !_.isEmpty(opts.url) && _.is
    // this.domain = opts.url && _.isString(opts.url) && !_.isEmpty(opts.url) && _.is
    //  this.parsedUrl
    //  this.parsedUrl
    //  'http://jsonplaceholder.typicode.com'
    // 'api/posts'
    // 'http://jsonplaceholder.typicode.com/api/posts/'
    // 'http://jsonplaceholder.typicode.com/api/posts/resource'

    // 'http://jsonplaceholder.typicode.com/api/posts/:id'
    // 'http://jsonplaceholder.typicode.com/api/posts/:id/resource'
    // 'http://jsonplaceholder.typicode.com/api/posts/:id/resource/:resourceId/nested'

    // 'http://jsonplaceholder.typicode.com/api/posts'
    // 'http://jsonplaceholder.typicode.com/api/posts/active'
    // 'http://jsonplaceholder.typicode.com/api/posts/1'
    // 'http://jsonplaceholder.typicode.com/api/posts/1/resource'
    // 'http://jsonplaceholder.typicode.com/api/posts/1/resource/2/other'

// { resource: '1', subresource: 'method', nested: '2', _: 'method' }
  }

  get request() {
    return agent;
  }
}

ApiRequest.superagent = request;

ApiRequest.prototype.head = function () {

  return agent.head();
};

// returns one resource record
ApiRequest.prototype.get = function () {

  return agent.get();
};

ApiRequest.prototype.post = function () {
  return agent.post();
};

ApiRequest.prototype.put = function () {
  return agent.put();
};

ApiRequest.prototype.delete = function () {
  return agent.delete();
};

module.exports = ApiRequest;
